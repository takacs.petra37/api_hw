package com.epam.training.test_backend.model;

import java.util.List;

import com.epam.training.test_backend.framework.CreateJSONBody;

public class Bet extends CreateJSONBody {
	
	// here comes the bet model
	
	private Integer id;
	private String description;
	private String type;
	
	public Bet()  {		
	}	
		
	public Bet(Integer id, String description, String type) {
		super();
		this.id = id;
		this.description = description;
		this.type = type;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
	
}
