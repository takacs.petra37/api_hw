package com.epam.training.test_backend.model;

import java.util.List;

import com.epam.training.test_backend.framework.CreateJSONBody;

public class Event extends CreateJSONBody {
	
	// here comes the event model
	private Integer id;
	private String title;
	private String type;
	private List<Integer> start;
	private List<Integer> end;
	
	public Event()  {		
	}	
		
	public Event(Integer id, String title, String type, List<Integer> start, List<Integer> end) {
		super();
		this.id = id;
		this.title = title;
		this.type = type;
		this.start = start;
		this.end = end;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<Integer> getStart() {
		return start;
	}
	public void setStart(List<Integer> start) {
		this.start = start;
	}
	public List<Integer> getEnd() {
		return end;
	}
	public void setEnd(List<Integer> end) {
		this.end = end;
	}
}
