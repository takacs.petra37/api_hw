package com.epam.training.test_backend.tests;

import static org.junit.Assert.assertEquals;

import org.json.JSONException;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;

import com.epam.training.test_backend.endpointactions.Events;
import com.epam.training.test_backend.endpointactions.Players;
import com.epam.training.test_backend.framework.BasicTest;
import com.epam.training.test_backend.model.Bet;
import com.epam.training.test_backend.model.Event;
import com.epam.training.test_backend.model.Player;

import io.restassured.response.Response;

import com.google.gson.*;

public class SportsBettingBackEndTest extends BasicTest {

	@Test
	public void verifyUserRegistrationTest() throws JSONException {
		Response returnedPlayer = Players.getPlayerById(userId, sessionId);
		
		Player player = new Player.Builder(Integer.parseInt(userId))
				.withUserName(userName)
				.withName(TEST_NAME)
				.withAccountNumber(accountNumber)
				.withBalance(BALANCE)
				.withCurrency(HUF_CURRENCY)
				.build();
		
		JSONAssert.assertEquals(player.createJSONBodyWithNulls(), returnedPlayer.asString(), true);
	}
	
	@Test
	public void modifyUserTest() throws JSONException {
		Player player = new Player.Builder(Integer.parseInt(userId))
				.withUserName(userName)
				.withName("modifiedNameHere") // modified
				.withAccountNumber(accountNumber)
				.withBalance(42000) // modified
				.withCurrency(HUF_CURRENCY)
				.build();
		
		// update the player here!
		Players.updatePlayerById(player, sessionId);
		
		Response returnedPlayer = Players.getPlayerById(userId, sessionId);

		// this is an application DB logic here
		player.setVersion(player.getVersion() + 1);
		
		JSONAssert.assertEquals(player.createJSONBodyWithNulls(), returnedPlayer.asString(), true);
	}
	
	@Test
	public void homeworkTest() {
		int actualNumberOfBets = 0;
		int actualNumberOfEvents = 0;
		
		// get the event and verify the number of them
		Response returnedEvents = Events.getEvents(sessionId);
		
		//returnedEvents.asString()
		
		Gson gson = new Gson();
		Event[] events = gson.fromJson(returnedEvents.asString(), Event[].class);
		actualNumberOfEvents = events.length;
		
		assertEquals("There must be only one event!", 1, actualNumberOfEvents);
		
		// get the bets of the previous event (by its ID) and verify the number of them
		Response returnedBets = Events.getBetsByEventId(events[0].getId(), sessionId);
		
		Bet[] bets = gson.fromJson(returnedBets.asString(), Bet[].class);
		actualNumberOfBets = bets.length;
		
		assertEquals("There must be two bets!", 2, actualNumberOfBets);
	}
	
}
